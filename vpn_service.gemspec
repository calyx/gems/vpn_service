Gem::Specification.new do |spec|
  spec.name        = "vpn_service"
  spec.version     = "0.0.1"
  spec.authors     = ["Petal Developers"]
  spec.summary     = "VPN Service For Petal"
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*"]
  end

  spec.add_dependency "certificate_authority"
end
