#
# OpenVPN X509 Client Certificate
#
require 'base64'
require 'digest/md5'
require 'openssl'
require 'certificate_authority'
require 'date'
require 'securerandom'

module Vpn
  class ClientCertificate
    attr_accessor :prefix, :common_name,
                  :expires_on, :bit_size, :cert_hash, :ca_cert_path, :ca_key_path

    def initialize(options = {})
      @prefix = options[:prefix] || "client-"
      @common_name = options[:common_name] || SecureRandom.uuid
      @expires_on = options[:expires_on] || last_month + 36500 # 100 years from now
      @bit_size = options[:bit_size] || 2024
      @cert_hash = options[:cert_hash] || "SHA256"
      @ca_cert_path = options[:ca_cert]
      @ca_key_path = options[:ca_key]
    end

    def cert
      generate_cert unless @cert
      @cert
    end

    def key
      generate_cert unless @key
      @key
    end

    def to_s
      self.key.to_pem + self.cert.to_pem
    end

    def fingerprint
      OpenSSL::Digest::SHA1.hexdigest(openssl_cert.to_der).scan(/../).join(':')
    end

    private

    def generate_cert
      @cert = CertificateAuthority::Certificate.new

      @cert.subject.common_name = [@prefix,@common_name].join
      @cert.not_before = to_time(last_month())
      @cert.not_after  = to_time(@expires_on)

      # generate key
      @cert.serial_number.number = cert_serial_number()
      @cert.key_material.generate_key(@bit_size)

      # sign
      @cert.parent = ClientCertificate.root_ca(@ca_cert_path, @ca_key_path)
      @cert.sign! client_signing_profile()

      @key = cert.key_material.private_key
    end

    def openssl_cert
      cert.openssl_body
    end

    def self.root_ca(ca_cert_path, ca_key_path, ca_key_password:nil)
      @root_ca ||= begin
        crt = File.read(ca_cert_path)
        key = File.read(ca_key_path)
        cert = CertificateAuthority::Certificate.from_openssl(OpenSSL::X509::Certificate.new(crt))
        cert.key_material.private_key = OpenSSL::PKey::RSA.new(key, ca_key_password)
        cert
      end
    end

    #
    # For cert serial numbers, we need a non-colliding number less than 160 bits.
    # md5 will do nicely, since there is no need for a secure hash, just a short one.
    # (md5 is 128 bits)
    #
    def cert_serial_number
      Digest::MD5.hexdigest("#{rand(10**10)} -- #{Time.now}").to_i(16)
    end

    def client_signing_profile
      {
        "digest" => cert_hash,
        "extensions" => {
          "keyUsage" => {
            "usage" => ["digitalSignature"]
          },
          "extendedKeyUsage" => {
            "usage" => ["clientAuth"]
          }
        }
      }
    end

    #
    # the first day of the last month
    #
    def last_month
      month = Date.today.prev_month
      Date.new(month.year, month.month, 1)
    end

    def to_time(date)
      return date if date.is_a? Time
      Time.new(date.year, date.month, date.day, 0, 0, 0, "+00:00")
    end

  end
end